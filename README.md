# Daka

> 个人打卡记录检测工具

## Installation

-   要求：

    -   系统：windows/*nix

    -   python版本：python2.7

-   安装：

        pip install -r requirements.txt

## Usage

-   运行方式有两种：

    1.  直接运行：

            python daka.py

    2.  生成可执行文件后运行：

            pyinstaller daka.spec

        该命令会生成一个可执行文件，如果是 windows 平台，则对应的是 dist/daka/daka.exe，
        如果是 *nix 平台，则对应的是 dist/daka/daka

-   运行后按照提示操作即可，第一次使用需要填写登录的用户名和密码
